from pathlib import Path
import json

COUNT = 2

class Person:
    def __init__(self, id):
        self.id = int(id)
        data = Path("db.json").read_text()
        data = json.loads(data)
        self.name = data[id]["name"]
        self.age = data[id]["age"]
        self.school = data[id]["school"]
        self.professions = data[id]["professions"]

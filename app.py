from flask import Flask, render_template

from models import Person, COUNT
from views import one_person, multi_people

app = Flask(__name__)

@app.route("/")
def hello():
    return render_template("home.html")

@app.route("/<int:id>")
def detailed(id):
    person = Person(str(id))
    return one_person(person)

@app.route("/all")
def all_people():
    people = []
    for i in range(1, COUNT+1):
        people.append(Person(str(i)))
    return multi_people(people)

if __name__ == "__main__":
    app.run(port=5000, debug=True)
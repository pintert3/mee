from flask import render_template

def one_person(person):
    return render_template("details.html", person=person)

def multi_people(people):
    return render_template("people.html", people=people)